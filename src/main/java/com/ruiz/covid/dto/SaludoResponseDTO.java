package com.ruiz.covid.dto;

public class SaludoResponseDTO {
	private String sayHello;
	private int edad;

	public String getSayHello() {
		return sayHello;
	}

	public void setSayHello(String sayHello) {
		this.sayHello = sayHello;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}
}
