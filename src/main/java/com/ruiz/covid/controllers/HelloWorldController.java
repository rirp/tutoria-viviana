package com.ruiz.covid.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ruiz.covid.dto.SaludoRequestDTO;
import com.ruiz.covid.dto.SaludoResponseDTO;

@RestController
public class HelloWorldController {
	
	@GetMapping("sayHello")
	public ResponseEntity<String> helloWorld() {
		return ResponseEntity.ok().body("Hola Mundo");
	}
	
	@PostMapping("addPerson")
	public ResponseEntity<String> addName(@RequestBody SaludoRequestDTO request) {
		if (request.getFrom() == null || request.getFrom().equals("")) {
			return ResponseEntity.badRequest().body("El valor de from no puede ser vacio");
		}
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body("Hola \nfrom: " + request.getFrom() + "\nto: " + request.getTo());
	}
	
	@PostMapping("hello")
	public ResponseEntity<SaludoResponseDTO> hello(@RequestBody SaludoRequestDTO request) {
		if (request.getFrom() == null || request.getFrom().equals("")) {
			return ResponseEntity.badRequest().body(null);
		}
		
		SaludoResponseDTO response = new SaludoResponseDTO();
		response.setSayHello("hola " + request.getTo() + ", soy " + request.getFrom());
		response.setEdad(10);
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(response);
	}
}
