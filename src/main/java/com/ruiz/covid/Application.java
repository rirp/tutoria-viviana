package com.ruiz.covid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(
		basePackages = {
				"com.ruiz.covid"
		})
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
